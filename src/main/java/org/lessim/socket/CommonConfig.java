package org.lessim.socket;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @date: 2021/12/2 4:46 下午
 * @author: Menjoe
 */
@Configuration
public class CommonConfig {

    @Bean
    @ConfigurationProperties(prefix = "endpoint")
    public IMNode imConfigBean() {
        return new IMNode();
    }

}
