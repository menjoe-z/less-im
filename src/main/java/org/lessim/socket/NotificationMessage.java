package org.lessim.socket;

import lombok.Data;

@Data
public class NotificationMessage {
    /**
     * 发送者账号
     */
    public Long from;
    /**
     * 发送者名称
     */
    public String fromName;
    /**
     * 接收者账号
     */
    public Long to;
    /**
     * 发送的内容
     */
    public String text;
    /**
     * 发送的日期
     */
    public Long date;

}