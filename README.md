# less-im

#### 介绍
使用spring websocket 写的一个简单 IM

#### 软件架构
软件架构说明


#### 安装教程

1.  git clone https://gitee.com/menjoe-z/less-im.git  
2.  mvn clean install 编译项目

#### 使用说明

1.  导入到 idea 或者 eclipse
2.  运行 org.lessim.Application
3.  使用 WebSocket 客户端连接

#### JS WebSocket 示例

```
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <script src="https://cdn.jsdelivr.net/npm/sockjs-client@1/dist/sockjs.min.js"></script>

</head>
<body>


<button onclick="sendMsg()">发送消息</button>

<script type="text/javascript">

    let from = "5521";
    let fromName = '自己';
    let to = 5521;
    var webSocket = new SockJS("http://localhost:9001/websockets?uid=" + from);

    webSocket.heartbeatTime = 10;

    webSocket.onopen = function (event) {
        console.log("WebSocket:已连接");
    };
    webSocket.onerror = function (event) {
        console.log("WebSocket:发生错误 ");
        console.log(event);
    };
    webSocket.onclose = function (event) {
        console.log("WebSocket:已关闭");
        console.log(event);
    };
    webSocket.onmessage = function (event) {
    // 接收到的消息的对象
        // let data = JSON.parse(event.data);
        console.log(event.data)
    };
    // 发送消息的实例
    function sendMsg() {
        let data = {};
        data["from"] = from;
        data["fromName"] = fromName;
        data["to"] = to;
        data["text"] = "我发给你一条信息";
        webSocket.send(JSON.stringify(data));
    }
</script>

</body>
</html>
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
