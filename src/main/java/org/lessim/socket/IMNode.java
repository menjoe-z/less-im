package org.lessim.socket;

import lombok.Data;

@Data
public class IMNode {

    private String handler;

    private String[] allowedOrigins;

    private long heartBeat;

}
